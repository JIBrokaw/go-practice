package agent

import (
  "gitlab.com/akita/akita/v3/sim"
)

type Order struct{
  sim.MsgMeta
  Quantity int
}

func (order *Order) Meta() *sim.MsgMeta{
  return &order.MsgMeta
}

func NewOrder(quantity int) *Order{
  return &Order{MsgMeta: sim.MsgMeta{}, Quantity: quantity}
}

type Shipment struct{
  sim.MsgMeta
  Quantity int
}

func (ship *Shipment) Meta() *sim.MsgMeta{
  return &ship.MsgMeta
}

func NewShipment(quantity int) *Shipment{
  return &Shipment{MsgMeta: sim.MsgMeta{}, Quantity: quantity}
}
