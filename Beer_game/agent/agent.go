package agent

import (
  "fmt"
  "gitlab.com/akita/akita/v3/sim"
)

type Agent struct{
  *sim.TickingComponent

  UpStream sim.Port
  DownStream sim.Port

  UpStreamAgent sim.Port
  DownStreamAgent sim.Port

  Inventory int
  Backlog int
  OnOrder int
  InventoryCost int
  LostCustomerPenalty int
  IsRetailer bool
  IsFactory bool
}

func (agent *Agent) Tick(now sim.VTimeInSec) bool{
  fmt.Println("\nLet's try running", agent.Name(), "at time", now)
  progressMade := false
  // receive order
  var rcvdOrder = agent.DownStream.Retrieve(now)
  if rcvdOrder != nil{
    var order = rcvdOrder.(*Order)
    // var order = Order(rcvdOrder)
    agent.Backlog += order.Quantity
    fmt.Println(agent.Name(), "received order for ", order.Quantity, ". Its backlog is now", agent.Backlog)
    progressMade = true
  }
  // receive shipment
  var rcvdShipment = agent.UpStream.Retrieve(now)
  if rcvdShipment != nil{
    var ship = rcvdShipment.(*Shipment)
    agent.Inventory += ship.Quantity
    agent.OnOrder -= ship.Quantity
    fmt.Println(agent.Name(), "received shipment for ", ship.Quantity, ". Its on order is now", agent.OnOrder)
    progressMade = true
  }
  // send shipment
  if agent.Inventory > 0 || agent.Backlog > 0{
    var quantity = 0
    if agent.Inventory < agent.Backlog{
      quantity = agent.Inventory
    } else{
      quantity = agent.Backlog
    }
    if quantity != 0{
      if !agent.IsRetailer{
        var shipToSend = NewShipment(quantity)
        shipToSend.Src = agent.DownStream
        shipToSend.Dst = agent.DownStreamAgent
        shipToSend.SendTime = now //+ sim.VTimeInSec(1)
        agent.DownStreamAgent.Send(shipToSend)
      }else{
        fmt.Println("Retailer satisfied", quantity, "customers!")
      }
      agent.Inventory -= quantity
      agent.Backlog -= quantity
      fmt.Println(agent.Name(), "sent", quantity, ". Its inventory is now ", agent.Inventory, ". Its backlog is now", agent.Backlog, ".")
      progressMade = true
    }
  }
  // send order
  var quantityDesired = agent.Backlog - agent.Inventory/agent.InventoryCost - agent.OnOrder + 48/agent.InventoryCost// 34 - agent.InventoryCost
  if quantityDesired > 0{
    if agent.IsFactory{
      agent.Inventory += quantityDesired
      fmt.Println("Factory made ", quantityDesired)
    } else{
        var orderToRequest = NewOrder(quantityDesired)
        orderToRequest.Src = agent.UpStream
        orderToRequest.Dst = agent.UpStreamAgent
        orderToRequest.SendTime = now // + sim.VTimeInSec(1)
        agent.UpStreamAgent.Send(orderToRequest)
        agent.OnOrder += quantityDesired
        fmt.Println(agent.Name(), "requested", orderToRequest.Quantity, "from ", agent.UpStreamAgent.Name())
    }
    progressMade = true
  }

  agent.InvokeHook(sim.HookCtx{Domain:agent, Pos: EndDayHookPos})
  fmt.Println(agent.Name(), " progress was", progressMade)
  return progressMade
}

func NewAgent(engine sim.Engine, name string) *Agent{
  agent := new(Agent)
  agent.TickingComponent = sim.NewTickingComponent(name, engine, 1*sim.GHz, agent)
  switch name{
    case "Retailer":
      agent.InventoryCost = 1
    case "Wholesaler":
      agent.InventoryCost = 2
    case "Distributor":
      agent.InventoryCost = 3
    case "Factory":
      agent.InventoryCost = 4
    }
  agent.LostCustomerPenalty = 30
  agent.UpStream = sim.NewLimitNumMsgPort(agent, 4, name+".UpStream")
  agent.DownStream = sim.NewLimitNumMsgPort(agent, 4, name+".DownStream")
  return agent
}
