
package main

import "fmt"

var list [4]int

var target int = 26

func main(){
  list[0] = 2
  list[1] = 7
  list[2] = 11
  list[3] = 15

  numbers_seen := make(map[int]int)

  //Time complexity O(n)
  //Because it walks through the values once for O(n)
  //and checks the complement (target-value) for each value in O(1) time to see if it's already been seen
  for i := 0; i < 4; i++ {
    var v, found = numbers_seen[target - list[i]]
    if found{
      fmt.Println("The values", list[i], "and", v, "add up to", target)
      return
    }
    numbers_seen[list[i]] = list[i]
  }
  fmt.Println("There are no two numbers that add up to", target)

}
