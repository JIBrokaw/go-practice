package main

import "fmt"

type Database struct{
  students []*Student
}

func NewDatabase() *Database{
  student_list := make([]*Student, 0)
  return &Database{student_list}
}

func add_student(d *Database, name, id string) *Student{
  newStudent := NewStudent(name, id)
  d.students = append(d.students, newStudent)
  fmt.Println("Student added")
  return newStudent
}

func add_student_full(d *Database, name, id, major string, age int) *Student{
  newStudent := NewStudentFull(name, id, major, age)
  d.students = append(d.students, newStudent)
  fmt.Println("Student added")
  return newStudent
}

func find_student_by_id(d *Database, id string) *Student{
  for i := range d.students{
    if d.students[i].id == id{
      return d.students[i]
    }
  }
  return nil
}

func find_student_by_name(d *Database, name string) *Student{
  for i := range d.students{
    if d.students[i].name == name{
      return d.students[i]
    }
  }
  return nil
}

func find_students_by_course_name(d *Database, course string) []*Student{
  students_found := make([]*Student, 0)
  for i := range d.students{
    for j := range d.students[i].courses{
        if d.students[i].courses[j].name == course{
          students_found = append(students_found, d.students[i])
          break
        }
    }
  }
  return students_found
}

func num_student(d *Database) int{
  return len(d.students)
}
