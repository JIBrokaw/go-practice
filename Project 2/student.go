package main

import "fmt"

type Course struct{
  name string
  hours int
  grade float64
}

func NewCourse(name string, hours int, grade float64) *Course{
  return &Course{name, hours, grade}
}

type Student struct{
  name string
  id string
  major string
  age int
  courses []*Course
}

func addCourse(s *Student, course_name string, credit_hours int, grade float64) {
  var course *Course = NewCourse(course_name, credit_hours, grade)
  s.courses = append(s.courses, course)
}

func calculateGpa(s *Student) float64{
  num_classes := len(s.courses)
  fmt.Println(num_classes)
  if num_classes == 0{
    return 0
  }
  var gpa float64
  var total_hours int
  for i := range s.courses{
    gpa += float64(s.courses[i].hours) * s.courses[i].grade
    total_hours += s.courses[i].hours
  }
  return gpa/float64(total_hours)
}

func NewStudent(name, id string) *Student{
  course_list := make([]*Course, 0)
  return &Student{name, id, "", 0, course_list}
}

func NewStudentFull(name, id, major string, age int) *Student{
  course_list := make([]*Course, 0)
  return &Student{name, id, major, age, course_list}
}
