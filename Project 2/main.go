
package main

func main(){
  var db = NewDatabase()
  var io = NewIO()
  var pg = NewProgram(db, io)
  run(pg)
}
